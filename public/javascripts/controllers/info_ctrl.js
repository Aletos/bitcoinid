function InfoController($scope, $http){
	$scope.info = {
		num_addresses: 0,
		num_entities: 0,
		num_blocks_synced: 0
	};

	
	$scope.hasData = false;
	$scope.error = false;
	
	$scope.getInfo = function(){
		$http({method : 'GET',url : '/api/info'})
            .success(function(data, status) {
                $scope.info = data;
                $scope.hasData = true;
            })
            .error(function(data, status) {
                $scope.error = "Problems getting overview info. Status = " + status;
            });
	}
	
	$scope.calcPercentSynced = function(){
		var info = $scope.info;
		if(!info.latest_block || !info.num_blocks_synced)
			return "0%";
		
		var val =  ( info.num_blocks_synced) / (info.latest_block.height + 1.0) * 100;
		val =  val.toFixed(2);
		return val + "%";
	}
	
	$scope.calcNumBlocksRemaining =function(){
		var info = $scope.info;
		if(!info.latest_block || !info.num_blocks_synced)
			return 0;
		else
			return info.latest_block.height - info.last_block_mapped;
	}
}
