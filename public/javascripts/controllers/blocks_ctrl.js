function BlocksController($scope, $http){
	$scope.blocks = [];
	$scope.hasData = false;
	$scope.error = false;
	
	$scope.getBlocks = function(numBlocks){
		$http({method : 'GET',url : '/api/blocks'})
            .success(function(data, status) {
            	console.log(data);
                $scope.blocks = data;
                $scope.hasData = true;
            })
            .error(function(data, status) {
                $scope.error = "Problems getting blocks. Status = " + status;
            });
	}
	
	$scope.getNumBlocks = function(){
		return $scope.blocks.length;
	}
	
	$scope.getBlockDateTime = function(block){
		return timeConverter(block.time);
	}
}

function timeConverter(UNIX_timestamp){
	 var a = new Date(UNIX_timestamp*1000);
	 var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
	 var year = a.getFullYear();
	 var month = months[a.getMonth()];
	 var date = a.getDate();
	 var hour = a.getHours();
	 var min = a.getMinutes();
	 var sec = a.getSeconds();
	 var time = month+' '+date+', '+year+' '+hour+':'+min+':'+sec ;
	 return time;
}