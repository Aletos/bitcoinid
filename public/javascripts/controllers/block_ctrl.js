function BlockController($scope, $http){
	$scope.block = {};
	$scope.hasData = false;
	$scope.error = false;
	
	$scope.getBlock = function(blockIndex){
		$http({method : 'GET',url : '/api/block/' + blockIndex})
            .success(function(data, status) {
            	console.log(data);
                $scope.block = data;
                $scope.hasData = true;
                $scope.populateTransactions();
            })
            .error(function(data, status) {
                $scope.error = "Problems getting block " + blockIndex + ". Status = " + status;
            });
	}
	
	
	$scope.populateTransactions = function(){
		
		for(var k in $scope.block.tx){
			var tx = $scope.block.tx[k];
			
			$http({method : 'GET',url : '/api/transactions/' + tx + "/with/entities"})
            .success(function(data, status) {
                var idx = $scope.block.tx.indexOf(data.txid);
                console.log('idx = ' + idx);
                $scope.block.tx[idx] = data;
            })
            .error(function(data, status) {
                $scope.error = "Problem while populating transaction. Status = " + status;
            });
			
		}
		
		
	}
	
	
	$scope.getBlockDateTime = function(block){
		return timeConverter(block.time);
	}
}
