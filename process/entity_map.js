var mongoose = require("mongoose");
var Info = mongoose.model("Info");
var Address = mongoose.model("Address");
var Entity = mongoose.model("Entity");
var blockchain = require("../models/blockchain");

/**
 * Does Union Find on transaction inputs to merge entities together. It looks at the transactions
 * for the main_chain block at block height: info.last_block_mapped 
 * @param {Function} cb: callback(error)
 */
function map_entities(cb){
	//First step is to figure out what blocks we have processed already
	Info.getInfo(function(err, info){
		if(err){
			console.log("Error while getting info record", err);
			return cb(err);
		}
		var block_height = info.last_block_mapped + 1;
		console.log("Going to grab blocks for block height: ", block_height);
		//Now we know which block needs to be processed next.  So we need to get the data for that block.
		blockchain.getBockByIndex(block_height, function(err, block){
			if(err){
				console.log("Error while getting blocks at block height", block_height);
				return cb(err);
			}
			console.log("Mapping block at block height ", block_height);
			if(block != null){
				map_block(block, function(err){
					if(err){
						console.log(err);
						return cb(err);
					} 
					console.log("Done mapping block");
					info.last_block_mapped = block_height;
					info.save(function(err, info){
						if(err) return cb(err);
						return cb();
					});
				});
				
			}
		});
	});
}

/**
 * Does all the mapping operation for the block.
 * 
 * @param {Object} block : Object representign a Bitcoin block
 * @param {Function} cb: callback(err)
 */
function map_block(block, cb){
	
	var num_done = 0;
	
	for(var k in block.tx){
		var tx = block.tx[k];
		
		blockchain.getRawTransaction(tx, function(err, tx){
			if(err) return cb(err);
			
			blockchain.populateTransactionAddresses(tx, function(err, tx){
				if(err) return cb(err);
				map_transaction(tx, function(err){
					if(err) return cb(err);
					num_done++;
					if(num_done == block.tx.length){
						return cb();	
					}
				});
			});
		});
		
	}
	
	
	
	
	
	
}


function map_transaction(tx, cb){
	var outputs_done = false;
	var inputs_done = false;
	var num_inputs_done = 0;
	
	var outputs = tx.out;
	map_outputs(outputs, function(err){
		if(err){
			console.log("Error when mapping output ", err);
			return cb(err);
		}
		outputs_done = true;
		if(outputs_done && inputs_done){ 
			cb();
		}
	});
	
	map_inputs(tx, function(err){
		if(err){
			console.log("Error when mapping input ", err);
			return cb(err);
		}
		inputs_done = true;
		
		if(outputs_done && inputs_done){ 
			cb();
		}
	});
}


/**
 * Maps each input component of a trasaction to an Address and an Entity.
 * After the input addresse are mapped the entity merge needs to happen.  All these input addresses should have the same entity.
 * 
 * @param {Array} tx : The transaction object. Looks at the `inputs` field.  The block reward has an empty input object.
 * @param {Function} cb: callback(err)
 */
function map_inputs(tx, cb){
	
	var inputs = tx.inputs;
	var num_done = 0;
	for(var k in inputs){
		var input = inputs[k];
		var addr = null;
		if(input.prev_out)
			addr = input.prev_out.addr;
		Address.createAddressIfNew(addr, function(err, address){
			if(err){
				console.log("Error occured while trying to map block", err);
				return cb(err);
			}
			num_done++;
			console.log("Input Address mapped ", address._id);
			if(num_done == inputs.length){
				//Need to do union on the inputs.
				Entity.union(tx, function(err, entity){
					if(err) return cb(err);
					if(entity){
						console.log("All input addresses for " + tx.hash + " merged to entity " + entity._id);
						
					}else{
						console.log("No entity merge done");
					}
					cb();
				});
			}
		});
		
	}
}

/**
 * Maps each output component of a trasaction to an Address and an Entity.
 * 
 * @param {Array} outputs : Array of output objects of a transaction that each need to be mapped.
 * @param {Function} cb: callback(err)
 */
function map_outputs(outputs, cb){
	var num_done = 0;
	for(var k in outputs){
		var output = outputs[k];
		Address.createAddressIfNew(output.addr, function(err, address){
			
			if(err){
				console.log("Error occured while trying to map block", err);
				return cb(err);
			}
			num_done++;
			console.log("Output Address mapped ", address._id);
			if(num_done == outputs.length){
				
				cb();
			}
		});
	}
	
}


function map_entities_driver(){
	map_entities(function(err){
		if(err) console.log(err);
		console.log("Done with iteration of map entities");
		setTimeout(map_entities_driver, 15000);
	});
}

//map_entities_driver();
