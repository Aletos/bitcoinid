var mongoose = require("mongoose");
var Info = mongoose.model("Info");
var Address = mongoose.model("Address");
var Entity = mongoose.model("Entity");
var blockchain = require("../models/blockchain");

/**
 * Does Union Find on transaction inputs to merge entities together. It looks at the transactions
 * for the main_chain block at block height: info.last_block_mapped 
 * @param {Function} cb: callback(error)
 */
function map_entities(cb){
	//First step is to figure out what blocks we have processed already
	Info.getInfo(function(err, info){
		if(err){
			console.log("Error while getting info record", err);
			return cb(err);
		}
		var block_height = info.last_block_mapped + 1;
		//Now we know which block needs to be processed next.  So we need to get the data for that block.
		blockchain.getBockByIndex(block_height, function(err, block){
			if(err){
				console.log("Error while getting blocks at block height", block_height);
				return cb(err);
			}
			console.log("Mapping blocks at block height ", block_height);
			if(block != null){
				map_block(block, function(err){
					if(err){
						console.log(err);
						return cb(err);
					} 
					console.log("Done mapping block");
					info.last_block_mapped = block_height;
					info.save(function(err, info){
						if(err) return cb(err);
						return cb();
					});
				});
				
			}
		});
	});
}

/**
 * Does all the mapping operation for the block.
 * 
 * @param {Object} block : Object representign a Bitcoin block
 * @param {Function} cb: callback(err)
 */
function map_block(block, cb){
	
	var num_outputs_done = 0;
	var num_inputs_done = 0;
	var num_done = 0;
	for(var t in block.tx){
		var tx_hash = block.tx[t]; 
		
		console.log(tx_hash);
		
		blockchain.getRawTransaction(tx_hash, function(err, tx){
			if(err) return cb(err);
			map_transaction(tx, function(err){
				if(err) return cb(err);
				num_done++;
				if(num_done == block.tx.length){
					cb();
				}
			})
		});
		
		//We now have 1 transactions.  We need to add all new addresses in its inputs and outputs.
		
		//Adding all the output address
		// var outputs = tx.out;
		// map_outputs(outputs, function(err){
			// if(err){
				// console.log("Error when mapping output ", err);
				// return cb(err);
			// }
			// num_outputs_done++;
// 			
			// if(num_inputs_done == block.tx.length && num_outputs_done == block.tx.length){ 
				// cb();
			// }
		// });
// 		
		// //Mapping all the input addresses
		// map_inputs(tx, function(err){
			// if(err){
				// console.log("Error when mapping input ", err);
				// return cb(err);
			// }
			// num_inputs_done++;
// 			
			// if(num_inputs_done == block.tx.length && num_outputs_done == block.tx.length){ 
				// cb();
			// }
// 			
		// });
	}
}

function map_transaction(tx, cb){
	
	map_vout(tx.vout, function(err){
		if(err) return cb(err);
		map_vin(tx.vin, function(err){
			if(err) return cb(err);
			console.log('it is ok');
			cb();
		})
	});
	
	
}


/**
 * Maps each input component of a trasaction to an Address and an Entity.
 * After the input addresse are mapped the entity merge needs to happen.  All these input addresses should have the same entity.
 * 
 * @param {Array} tx : The transaction object. Looks at the `inputs` field.  The block reward has an empty input object.
 * @param {Function} cb: callback(err)
 */
function map_inputs(tx, cb){
	
	var inputs = tx.inputs;
	var num_done = 0;
	for(var k in inputs){
		var input = inputs[k];
		var addr = null;
		if(input.prev_out)
			addr = input.prev_out.addr;
		Address.createAddressIfNew(addr, function(err, address){
			if(err){
				console.log("Error occured while trying to map block", err);
				return cb(err);
			}
			num_done++;
			console.log("Input Address mapped ", address._id);
			if(num_done == inputs.length){
				//Need to do union on the inputs.
				Entity.union(tx, function(err, entity){
					if(err) return cb(err);
					if(entity){
						console.log("All input addresses for " + tx.hash + " merged to entity " + entity._id);
						
					}else{
						console.log("No entity merge done");
					}
					cb();
				});
			}
		});
		
	}
}

function map_vin(vin, cb){
	
	
	for(var k in vin){
		var input = vin[k];
		if(input.txid){ //this is coins being spent.
			
			
		}
	}
}

function map_vout(vout, cb){
	var num_done = 0;
	for(var k in vout){
		var output = vout[k];
		if(output.scriptPubKey.addresses > 1){
			console.log("ERROR:  Found multiple addresses in pubScriptKey of vout");
			return cb("ERROR ERROR ERROR");
		}
		Address.createAddressIfNew(output.scriptPubKey.addresses[0], function(err, address){
			if(err){
				console.log("Error occured while trying to map block", err);
				return cb(err);
			}
			num_done++;
			console.log("Output Address mapped ", address._id);
			if(num_done == vout.length){
				cb();
			}
		});
	}
}

/**
 * Maps each output component of a trasaction to an Address and an Entity.
 * 
 * @param {Array} outputs : Array of output objects of a transaction that each need to be mapped.
 * @param {Function} cb: callback(err)
 */
function map_outputs(outputs, cb){
	var num_done = 0;
	for(var k in outputs){
		var output = outputs[k];
		Address.createAddressIfNew(output.addr, function(err, address){
			
			if(err){
				console.log("Error occured while trying to map block", err);
				return cb(err);
			}
			num_done++;
			console.log("Output Address mapped ", address._id);
			if(num_done == outputs.length){
				
				cb();
			}
		});
	}
	
}


function map_entities_driver(){
	blockchain.getBockByIndex(784, function(err, block){
		console.log(block);
	});
	map_entities(function(err){
		console.log("Done with iteration of map entities");
		setTimeout(map_entities_driver, 100);
	});
}

map_entities_driver();
