
/**
 * Module dependencies.
 */

var express = require('express');
var routes = require('./routes');
var user = require('./routes/user');
var blocks = require('./routes/blocks');
var http = require('http');
var path = require('path');

var db = require('./models/db');

var blocks_api = require("./routes/api/blocks");
var info_api = require("./routes/api/info");
var transaction_api = require("./routes/api/transactions");

var app = express();

// all environments
app.set('port', process.env.PORT || 3000);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.json());
app.use(express.urlencoded());
app.use(express.methodOverride());
app.use(app.router);
app.use(express.static(path.join(__dirname, 'public')));

// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}

app.get('/', routes.index);
app.get('/users', user.list);
app.get('/block/:id', blocks.findById);

//Blocks API
app.get("/api/blocks", blocks_api.find);
app.get("/api/block/:index", blocks_api.findByIndex);


//Info API
app.get("/api/info", info_api.index);

//Transactions API
app.get("/api/transactions/:txid/raw", transaction_api.getRawTransaction);
app.get("/api/transactions/:txid/with/entities", transaction_api.getEntityTransaction);
app.get("/api/transactions/:txid", transaction_api.getTransaction);


http.createServer(app).listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});


//Start background processes
require("./process/entity_map");
