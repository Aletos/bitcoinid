var http = require("http");
var blockchain = require("../../models/blockchain");
var mongoose = require("mongoose");
var Info = mongoose.model("Info");

exports.find = function(req, res, next){
	Info.getInfo(function(err, info){
		if(err) return next(err);
		var last_block = info.last_block_mapped;
		
		var num_done = 0;
		var recent_blocks = [];
		var total_needed = 0;
		for(var i = last_block; i >= 0 && i > last_block - 8; i--){
			total_needed++;
			blockchain.getBockByIndex(i, function(err, block){
				if(err) return next(err);
				recent_blocks.push(block);
				num_done++;
				if(num_done == total_needed){
					return res.json(recent_blocks);
				}
			});
		}
	})
};

exports.findByIndex = function(req, res, next){
	blockchain.getBockByIndex(parseInt(req.params.index), function(err, block){
		if(err) return next(err);
		res.json(block);
	});
};

