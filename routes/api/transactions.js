var blockchain = require("../../models/blockchain");
var mongoose = require("mongoose");

/**
 * GET method that returns the JSON form of the transaction.  This is a raw bitcoin transation.
 * It only has vin and vout.
 * @param {Object} req
 * @param {Object} res
 * @param {Object} next
 */
exports.getRawTransaction = function(req, res, next){
	var tx = req.params.txid;
	
	blockchain.getRawTransaction(tx, function(err, tx){
		if(err) return next(err);
		return res.json(tx);
	});
}

/**
 * Gets a transaction that has addresses filled in.  Particularly it has `inputs` and `out`.
 * @param {Object} req
 * @param {Object} res
 * @param {Object} next
 */
exports.getTransaction = function(req, res, next){
	var tx = req.params.txid;
	
	blockchain.getTransaction(tx, function(err, tx){
		if(err) return next(err);
		return res.json(tx);
	});
}


exports.getEntityTransaction = function(req, res, next){
	var tx = req.params.txid;
	
	blockchain.getEntityTransaction(tx, function(err, tx){
		if(err) return next(err);
		console.log(tx);
		return res.json(tx);
	})
}
