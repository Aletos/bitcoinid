var mongoose = require("mongoose");
var Info = mongoose.model("Info");
var Address = mongoose.model("Address");
var Entity = mongoose.model("Entity");
var Blockchain = require("../../models/blockchain");

/**
 * Gets the overview info as an object.
 */
exports.index = function(req, res, next){
	var data = {};
	
	Info.getInfo(function(err, info){
		if(err) return next(err);
		data.last_block_mapped = info.last_block_mapped;
		data.num_blocks_synced = info.last_block_mapped + 1;
		
		Entity.count(function(err, count){
			if(err) return next(err);
			data.num_entities = count;
			
			Address.count(function(err, count){
				if(err) return next(err);
				data.num_addresses = count;
				
				Blockchain.lastestBlock(function(err, block){
					if(err) return next(err);
					data.latest_block = block;
					data.latest_block
					res.json(data);
				})
			});
		});
	});
}
