var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var entitySchema = mongoose.Schema({
	relevant_transaction : {type : String}
	//union : {type: Schema.Types.ObjectId, ref : 'Entity'},
	//addresses_before_union : { type: [] }
});

/**
 * Takes a bitcoin transaction object and runs Find and Union on the input addresses.
 * All the addresses will now be mapped to the entity that is returned in the callback function.
 * 
 * @param {Object} tx: A transaction object to run entity union on the inputs of the transaction.
 * @param {Function} cb: callback(error, entity); entity is the new entity that all input addresses are now mapped to. Returns (null, null) if no union needed to be done.
 */
entitySchema.statics.union = function(tx, cb){
	var inputs = tx.inputs;
	var addresses = [];
	for(var k in inputs){
		var input = inputs[k];
		if(input.prev_out){
			addresses.push(input.prev_out.addr);
		}
	}
	
	
	if(addresses.length < 2){ //Nothing to union 
		return cb(null, null);
	}
	
	var Entity = this;

	var Address = mongoose.model('Address');
		

	var criteria = {
		 _id : {$in : addresses}
	};
	Address.find(criteria, function(err, addresses){
		if(err) return cb(err);
		
		var entities = [];
		for(var k in addresses){
			var address = addresses[k];
			entities.push(address.entity);
		}
		
		
		//We now have a list of all previous entities of the transaction inputs
		
		Address.find({entity : { $in : entities}}, function(err, addresses){
			if(err) return cb(err);
			//We now have a list of all addresses that belong to the person that made this transaction
			//We need to union this into the same entity.
			
			console.log("Union: addresses ", addresses);
			
			var entity_set = []; //Treat this as a set of entity ids.
			for(var k in addresses){
				var entity = addresses[k].entity + "";
				if(entity_set.indexOf(entity) == -1)
					entity_set.push(entity);
			}
			
			console.log("Number of unique entities in transanction input is ", entity_set);
			
			if(entity_set.length > 1){ //More than one entity was found.  So we need to make this all 1 entity.
				var entity = new Entity({});
				entity.save(function(err, entity){
					if(err) return cb(err);
					
					//The new entity is made. All the addresses need to point here now.
					var num_saved = 0;
					for(var k in addresses){
						var address = addresses[k];
						address.entity = entity._id;
						address.save(function(err, address){
							if(err) return cb(err);
							num_saved++;
							if(num_saved == addresses.length){
								
								//Need to delete old entity data
								Entity.remove({_id : {$in : entity_set}}, function(err){
									if(err) return cb(err);
									return cb(null, entity);
								});
								
							}
						});
					}
					
				});
			}
			else{
				console.log("Didn't find anything to merge.");
				cb(null, null);
			}
		});
		
		
	});
	
	
	
	// entity.save(function(err, entity){
		// if(err) return cb(err);
// 		
// 		
// 		
		// var criteria = {
			// _id : addresses
		// };
// 		
		// Address.find(criteria).populate('entity').exec(function(err, addresses{
			// if(err) return cb(err);
// 			
// 			
			// cb(null, entity);
		// });
	// });
}


var Entity = mongoose.model('Entity', entitySchema);

