var http = require("http");
var bitcoin = require('bitcoin');
var kapitalize = require('kapitalize')();
var mongoose = require("mongoose");

kapitalize.auth('bitcoinid', '9CCCBD40CFC4609D871FD32E05ECA8138DBC5D2945CDFA9CA08CBA504DF31A17');

/**
 * Note that this is not a Mongoose collection but rather functions to get data from blockchain and blockchain.info
 */


/**
 * Gets a list of blocks that have the given blockheight.
 * Uses blockchain.info api.
 * 
 * @param {Object} blockHeight
 * @param {Object} cb: callback(err, blocks)
 */
exports.blocksByHeight = function(blockHeight, cb){
	var options = {
	  host: 'blockchain.info',
	  port: 80,
	  path: '/block-height/'+ blockHeight +'?format=json',
	  method: 'GET'
	};
	http.get(options, function(resp){
		var body = '';
	  	resp.on('data', function(chunk){
	    	body += chunk;
	 	 });
	  	resp.on("end", function(){
	  		var data = JSON.parse(body);
	  		if(data.blocks){
	  			data = data.blocks;
	  		}else{
	  			data = [];
	  		}
			cb(null, data);
	  	});
	}).on("error", function(e){
		cb("1000 " + e);
	});
}

/**
 * Returns the client for the bitcoind rpc server. 
 */
//var bitcoin_client = null;
function getBitcoinClient(){
	/*if(bitcoin_client){
		
		return bitcoin_client;
			
	}*/
	/*var bitcoin_client = new bitcoin.Client({
	  host: '127.0.0.1',
	  port: 8332,
	  user: 'bitcoinid',
	  pass: '9CCCBD40CFC4609D871FD32E05ECA8138DBC5D2945CDFA9CA08CBA504DF31A17'
	});

	return bitcoin_client;*/
}

/**
 * Gets the block from bitcoind that hsa that index
 * 
 * @param {Integer} blockIndex
 * @param {Function} cb : callback(err, block) 
 */
exports.getBockByIndex = function(blockIndex, cb){
	//var client = getBitcoinClient();
	kapitalize.getBlockHash(blockIndex, function(err, blockHash) {
	 	if (err) {
			return cb("1001: " + err);
		}
		console.log(blockHash);
		kapitalize.getBlock(blockHash, function(err, block){
			if(err) return cb("1002: " + err);
			cb(null, block);
		});
	});	
}

exports.getBockByIndexWithTXs = function(blockIndex, cb){
	//var client = getBitcoinClient();
	var module = this;
	
	this.getBockByIndex(blockIndex, function(err, block){
		if(err) return cb("1003: " + err);
		module.populateBlockTransactions(block, function(err, block){
			if(err) return cb("1004: " + err);
			cb(null, block);
		});
		
		
	});
}

/**
 * Given a bitcoin block from the bitcoind populate the transaction array from its list of trasnsaction hashes. 
 * 
 * @param {Function} cb(error, block). Block is the block with tx replaced by a populated list of transactions.
 */
exports.populateBlockTransactions = function(block, cb){
	if(!block.tx){
		cb("1005: This block doesn't have a transaction array");
	}
	var module = this;
	var num_done = 0;
	var transactions = [];
	for(var k in block.tx){
		var tx = block.tx[k];
		this.getRawTransaction(tx, function(err, transaction){
			if(err) return cb("1006: " + err);
		
			
			module.populateTransactionAddresses(transaction, function(err, tx){
				if(err) return cb("1007: " + err);
				num_done++;
				transactions.push(tx);
				if(num_done == block.tx.length){
					block.tx = transactions;
					cb(null, block);
				}
				
			});
			
			
		});
	}
}

/**
 * Given a bitcoin transaction with vin and vout go through and populate the addresses for inputs and out
 * 
 * @param {Object} tx.  A bitcoin transaction object from bitcoind.
 * @param {Function} cb. callback(err, tx). Where tx will be the original transaction with the `inputs` and `out` fields filled in. 
 */
exports.populateTransactionAddresses = function(tx, cb){
	var module = this;
	var vin = tx.vin;
	var vout = tx.vout;
	
	tx.out = [];
	for(var k in vout){
		var out = vout[k];
		var output = {};
		output.value = out.value;
		output.n = out.n;
		output.addr = out.scriptPubKey.addresses[0];
		tx.out.push(output);
	}
	
	tx.inputs = [];
	
	var num_done = 0;
	var vout_mapping = {};
	for(var k in vin){
		var inp = vin[k];
		if(inp.txid){
			vout_mapping[inp.txid] = inp.vout;
			module.getRawTransaction(inp.txid, function(err, trans){
				if(err) return cb("1008: " + err);
				var input = {};
				input.prev_out = {};
				input.prev_out = trans.vout[vout_mapping[trans.txid]];
				input.prev_out.addr = input.prev_out.scriptPubKey.addresses[0];
				delete input.prev_out.scriptPubKey;
				tx.inputs.push(input);
				num_done++;
				if(num_done == vin.length){
					cb(null, tx);
				}
			});
		}
		else{ //this is a coinbase transaction
			tx.inputs.push({});
			num_done++;
			
			if(num_done == vin.length){
				cb(null, tx);
			}
		}
		
	}
}

/**
 * Gets the transaction object for txid that has address and entity information populated. 
 */
exports.getEntityTransaction = function(txid, cb){
	var module = this;
	module.getTransaction(txid, function(err, tx){
		if(err) return cb(err);
		console.log("I have the transactoin " , tx.txid);
		var Address = mongoose.model("Address");
		
		
		console.log(tx.out);
		var addresses = [];
		for(var k in tx.out){
			addresses.push(tx.out[k].addr);
		}
		
		for(var k in tx.inputs){
			var input = tx.inputs[k];
			if(input.prev_out){
				addresses.push(input.prev_out.addr);
			}
		}
		
		console.log(addresses);
		var criteria = { _id : { $in : addresses } };
		console.log(criteria);
		Address.find(criteria, function(err, addresses){
			if(err) return cb(err);
			
			var addrmap = {}; //Maps addresses to entities
			for(var k in addresses){
				var addr = addresses[k];
				addrmap[addr._id] = addr.entity+"";
			}
			
			
			tx.eout = {}; //maps output entities to arrays of output addresses
			for(var k in tx.out){
				var addr = tx.out[k].addr;
				var entity = addrmap[addr];
				
				if(!tx.eout[entity]){
					tx.eout[entity] = [];
				}
				tx.eout[entity].push(tx.out[k]);
			}
			
			
			tx.ein = {}; //maps input entities to arrays of input addresses
			for(var k in tx.inputs){
				var input = tx.inputs[k];
				if(input.prev_out){
					var addr = input.prev_out.addr;
					var entity = addrmap[addr];
					if(!tx.ein[entity]){
						tx.ein[entity] = [];
					}
					tx.ein[entity].push(input);
				}
			}
			
			cb(null, tx);
			
			//tx.ein = {};
			
			
		});
	});
}

/**
 * Gives the transaction with addresses populated in. 
 */
exports.getTransaction = function(txid, cb){
	var module = this;
	module.getRawTransaction(txid, function(err, tx){
		if(err) return cb(err);
		module.populateTransactionAddresses(tx, function(err, tx){
			if(err) return cb(err);
			cb(null, tx);
		});
	});
}

exports.getRawTransaction = function(tx_hash, cb){
	//var client = getBitcoinClient();
	kapitalize.getRawTransaction(tx_hash, 1, function(err, tx) {
	 	if (err) {
			return cb("1009: " + err);
		}
		//console.log(tx.vout[0].scriptPubKey);
		cb(null, tx);
	});
}

/**
 * Gets the lastest block from the blockchain
 * @param {Function} cb: callback(error, block)
 */
exports.lastestBlock = function(cb){
	var options = {
	  host: 'blockchain.info',
	  port: 80,
	  path: '/latestblock',
	  method: 'GET'
	};
	http.get(options, function(resp){
		var body = '';
	  	resp.on('data', function(chunk){
	    	body += chunk;
	 	 });
	  	resp.on("end", function(){
	  		var data = JSON.parse(body);
			cb(null, data);
	  	});
	}).on("error", function(e){
		cb("1010: " + e);
	});
}


