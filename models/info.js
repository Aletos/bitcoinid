var mongoose = require('mongoose');

/**
 * There should only ever be one info record in this collection.  It is used to store stand alone values. 
 */
var infoSchema = mongoose.Schema({
	last_block_mapped : {type : Number, default : 0}
}, {collection: "info"});

/*
 * Gets the one record that this collection represents.
 * 
 * @param cb : callback(err, info)
 */
infoSchema.statics.getInfo = function(cb){
	this.findOne({}, function(err, info){
		if(err) return cb(err);
		return cb(null, info);
	});
}

var Info = mongoose.model('Info', infoSchema);


//Here we make sure there is 1 info record.
Info.getInfo(function(err, info){
	if(err) console.log("Error while verifying info record", err);
	
	if(info == null){
		info = new Info({});
		info.save(function(err, info){
			if(err) console.log("Error while verifying info record", err);
		});
	}
});
