var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var Entity = mongoose.model("Entity");

/**
 * This schema represents a Bitcoin Address. 
 */
var addressSchema = mongoose.Schema({
	_id : {type : String},
	entity : {type: Schema.Types.ObjectId, ref : 'Entity', index : true}
});

/**
 * If an address doesn't already exists for addr then it creates a new entity and address that are linked.
 * This is the preferred way to make sure an address exists in the db for a bitcoin address. This is because 
 * if an address needs to be created then so does an entity that maps to it.
 * 
 * @param {String} addr
 * @param {Object} cb: callback(err, address)
 */
addressSchema.statics.createAddressIfNew = function(addr, cb){
	if(addr == null){ //Can't do anything with a null address.
		return cb(null, { _id : "--- (Ignored) No Address ---"});
	}
	var Address = this;
	var data = {_id : addr};
	
	Address.findOne(data, function(err, address){
		if(err) return cb("1011: " + err);
		//console.log("Debug address = " , address);
		if(address == null){
			//Need to create an Entity and an address.
			var entity = new Entity({});
			entity.save(function(err, entity){
				if(err) return cb("1012: " + err);
				//We can finally create that address
				data.entity = entity._id;
				var new_addr = new Address(data);
				new_addr.save(function(err, address){
					if(err) {
						//Delete the entity so it isnt orphaned.
						Entity.remove({"_id" : entity._id}, function(err2){
							return cb("1013: " + err);
						});
					}else{
						return cb(null, address);
					}
					
					
				});
			});
		}
		else{
			return cb(null, address);
		}
	})
	
}


var Address = mongoose.model('Address', addressSchema);


