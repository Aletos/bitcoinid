var mongoose = require('mongoose');
var Entity = require('./entity');
var Address = require('./address');
var Info = require('./info');

mongoose.connect('mongodb://localhost/bitcoinid');

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function callback () {
	console.log("Connection to database successful");
});